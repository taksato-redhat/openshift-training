#!/bin/sh

USER_NAME=`id -un`

DEV_PROJECT=${USER_NAME}-dev
CICD_PROJECT=${USER_NAME}-cicd

# create project
oc new-project ${DEV_PROJECT}
oc new-project ${CICD_PROJECT}

# set permission
oc policy add-role-to-user edit system:serviceaccount:${CICD_PROJECT}:default -n ${DEV_PROJECT}

# deploy CI/CD components
oc process -f cicd-template.yaml -v DEV_PROJECT=${DEV_PROJECT} | oc create -f - -n ${CICD_PROJECT}
